Feature: Merchants refunds a transaction

  The customer needs to be able to get a refund on a transaction from a Merchant.

  Actors: Merchant

  Background:
    Given The costumer is created in the system with the cpr "123455-1234", name "Fred", lastname "Fredi" and with a balance of 0 kr
    Given I am created in the system with the cpr "654321-4322", name "Erdo", lastname "Erdog" and with a balance of 1000 kr


  Scenario:  The costumer request a refund on a transaction
    Given The costumer presents me with a unused token
    When I scan the token
    Then The token is valid and the costumer will be refunded 100kr with the message "Money refunded"

  Scenario: The customer requests a refund with an invalid token
    Given the costumer presents me with a invalid token
    When I scan the token
    Then the token is invalid and the costumer will not be refunded



