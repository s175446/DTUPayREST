Feature: Customer wants to use a token

  Background: Customer exists passes

    Scenario: I want to use a unused token and 200kr in my bank account
      Given I have 2 unused token
      When Merchant scans my token
      And Requests a transfer of 100kr
      Then I lose my token and the money is withdrawn from my account

  Scenario: I want to use a unused token and 50kr in my bank account
    Given I have 2 unused token
    When Merchant scans my token
    And Requests a transfer of 100kr
    Then I lose my token and get the message "Transaction error!"

  Scenario: I want to use a used token
    Given I have 1 used token
    When Merchant scans my token
    Then I get an error message "The token is not available in the system"

  Scenario: I want to use a token, while having none
    Given I have 0 used token
    Then I get an error message "The token is not available in the system"

  Scenario: I want to check my latest transaction log.
    Given I used a token and bought a bag for 100kr from a Merchant with the Token "Fk45h3r7".
    Then I should get a log of my latest transaction.