Feature: Checks if the user exists in the system

  Scenario: User successfully created in the system
    Given I create user with identifier "2222"
    Then User is created in the system with uniqID of "2222"
