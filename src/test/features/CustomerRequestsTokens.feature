Feature: Customer requests tokens
  Description: This feature generates token for the user

  Actor: Customer

  Background: Customer exists passes

  Scenario: I requests tokens for the first time
    Given I have 0 unused tokens
    When I request 5 tokens
    Then I should have 5 tokens

  Scenario: I request tokens with 2 or more unused tokens
    Given I have more than 2 unused tokens
    When I request 5 tokens
    Then I should get a TokenRequestDenied error message "You can't request tokens, while having more than 1 avaible"

  Scenario: I request more than 5 tokens
    Given I have less than 2 unused tokens
    When I request 6 tokens
    Then I should get a TokenRequestDenied error message "You can't request more than 5 tokens at a time"

  Scenario: I request less than 0 tokens
    Given I have less than 2 unused tokens
    When I request -1 tokens
    Then I should get a TokenRequestDenied error message "You can't request a negative amount of tokens"