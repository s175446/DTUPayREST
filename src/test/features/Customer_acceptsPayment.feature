Feature: Customer wants to pay
  Description: Customer wants to pay the amount the merchant has requested
  Actors: Customer

  Background: Customer exists passes

  Scenario: Customer wants to pay an amount of money he does have
  //assuming that we are over the token part
    Given I have 200 kr on my account
    When The merchant requests 100 kr payment
    Then I would get a success message with "You have transferred 100 kr to DTU store", and 100 kr will be subtracted from my account

  Scenario: Customer wants to pay an amount of money he doesn't have
    //assuming that we are over the token part
    Given I only have 50 kr on my account
    When The merchant requests 100 kr payment
    Then I would get an error message with "You don't have enough money on your account", and my balance would stay the same