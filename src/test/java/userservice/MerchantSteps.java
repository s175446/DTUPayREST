package userservice;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import dtu.ws.fastmoney.*;

import static org.junit.Assert.assertEquals;


public class MerchantSteps
{

    WebTarget tokenManagerUrl, accountManagerUrl;
    BankService bank = new BankServiceService().getBankServicePort();
    User consumer = new User();
    User merchant = new User();
    String token;
    Response verify;
    String consumerAccountID = "";
    String merchantAccountID = "";


    /**
     * @author Burhan
     * Instantiates target clients
     * tokenManagerURL = localhost:8383
     * accountManager = localhost: 8181
     */
    public MerchantSteps(){
        Client client = ClientBuilder.newClient();
        //baseUrl = client.target("http://localhost:8080/");
        tokenManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8383/");
        accountManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8181/");
    }

    @Before
    public void beforeScenario(){

    }

    /**
     * author: Burhan
     * Retires accounts after testing
     * Retire consumerAccountID
     * Retire merchantAccountID
     * @throws BankServiceException_Exception
     */
    @After
    public void afterScenario() throws BankServiceException_Exception {
        /*Response response = tokenManagerUrl.path("clearTokens/" + cpr).request().get(Response.class);
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        String tokens = getToken.readEntity(String.class);
        System.out.println("Reset request:" + response.getStatus());
        System.out.println("Reset tokens: " + tokens);
        System.out.println("Reset tokens: " + getToken.getStatus());*/
        if(!consumerAccountID.equals("")) {
            bank.retireAccount(consumerAccountID);
        }
        if(!merchantAccountID.equals("")){
            bank.retireAccount(merchantAccountID);
        }
    }

    /**
     * author: Alen
     * Tests existence of consunmer and his balance
     * @param cpr
     * @param name
     * @param lastname
     * @param amount
     * @throws BankServiceException_Exception
     */
    @Given("The costumer is created in the system with the cpr {string}, name {string}, lastname {string} and with a balance of {int} kr")
    public void theCostumerIsCreatedInTheSystemWithTheCprNameLastnameAndWithABalanceOfKr(String cpr, String name, String lastname, int amount) throws BankServiceException_Exception {
        consumer.setFirstName(name);
        consumer.setLastName(lastname);
        consumer.setCprNumber(cpr);

        AddCustomerBody body = new AddCustomerBody();
        body.cpr = cpr;
        body.name = name;

        String output = accountManagerUrl.path("addCustomers").request().post(Entity.entity(body, MediaType.APPLICATION_JSON), String.class);
        //accountManagerUrl.path("addCustomers").request().post(Entity.entity(consumer, MediaType.APPLICATION_JSON), String.class);
        consumerAccountID =  bank.createAccountWithBalance(consumer, new BigDecimal(amount));
    }

    /**
     * author: Burhan
     * Tests existence of merchant and his balance
     * @param cpr
     * @param name
     * @param lastname
     * @param amount
     * @throws BankServiceException_Exception
     */
    @Given("I am created in the system with the cpr {string}, name {string}, lastname {string} and with a balance of {int} kr")
    public void iAmCreatedInTheSystemWithTheCprNameLastnameAndWithABalanceOfKr(String cpr, String name, String lastname, int amount) throws BankServiceException_Exception {
        merchant.setFirstName(name);
        merchant.setLastName(lastname);
        merchant.setCprNumber(cpr);
        //accountManagerUrl.path("addCustomers").request().post(Entity.entity(merchant, MediaType.APPLICATION_JSON), String.class);
        merchantAccountID = bank.createAccountWithBalance(merchant, new BigDecimal(amount));
    }

    /**
     * Gets response with unused token
     * author: Erik
     * Read token
     */
    @Given("The costumer presents me with a unused token")
    public void theCostumerPresentsMeWithAUnusedToken() {
        Response response = tokenManagerUrl.path("requestToken/1/" + consumer.getCprNumber()).request().get(Response.class);
        token = response.readEntity(String.class);
    }

    /**
     * Test refund from merchant to consumer
     * author: Burhan
     * @param refundAmount
     * @param message
     * @throws BankServiceException_Exception
     */
    @Then("The token is valid and the costumer will be refunded {int}kr with the message {string}")
    public void theTokenIsValidAndTheCostumerWillBeRefundedKrWithTheMessage(int refundAmount, String message) throws BankServiceException_Exception {
        // Write code here that turns the phrase above into concrete actions
        BigDecimal refund = new BigDecimal(refundAmount);
        BigDecimal before = bank.getAccount(consumerAccountID).getBalance();
        BigDecimal beforeMerchant = bank.getAccount(merchantAccountID).getBalance();
        bank.transferMoneyFromTo(merchantAccountID, consumerAccountID, refund, message);
        BigDecimal after = bank.getAccount(consumerAccountID).getBalance();
        BigDecimal afterMerchant = bank.getAccount(merchantAccountID).getBalance();

        assertEquals(before.add(refund), after);
        assertEquals(beforeMerchant.subtract(refund), afterMerchant);
    }

    /**
     * Scans the token with the matching ID in the tokenDB
     * author: Alen
     */
    @When("I scan the token")
    public void iScanTheToken() {
        verify = tokenManagerUrl.path("useToken/" + token + "/" + consumer.getCprNumber()).request().get(Response.class);
    }

    /**
     * Tests if the token is invalid
     * author: Erik
     */
    @Given("the costumer presents me with a invalid token")
    public void theCostumerPresentsMeWithAInvalidToken() {
        Response getToken = tokenManagerUrl.path("requestToken/1/"+consumer.getCprNumber()).request().get(Response.class);
        token = getToken.readEntity(String.class);
        tokenManagerUrl.path("useToken/"+token+"/"+consumer.getCprNumber());
    }

    /**
     * Tests if response for the invalid token is not found
     * author: Alen
     */
    @Then("the token is invalid and the costumer will not be refunded")
    public void theTokenIsInvalidAndTheCostumerWillNotBeRefunded() {
        Response response = tokenManagerUrl.path("useToken/"+token+"/"+consumer.getCprNumber()).request().get(Response.class);
        assertEquals(response.getStatus(), 404);
    }
}
