package userservice;

import dtu.ws.fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

import static org.junit.Assert.*;

@XmlRootElement
class AddCustomerBody {
    @XmlElement
    String cpr;
    @XmlElement
    String name;

}

/**
 *
 */
public class CustomerSteps {

    WebTarget tokenManagerUrl, accountManagerUrl, logManagerUrl;
    String cpr = "2222";
    BankService bank = new BankServiceService().getBankServicePort();
    String consumerAccountID = "";
    String merchantAccountID = "";

    public CustomerSteps() {
        Client client = ClientBuilder.newClient();
        //baseUrl = client.target("http://fastmoney-07.compute.dtu.dk:8080/");
        tokenManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8383/");
        accountManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8181/");
        logManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:6969/");
    }

    /**
     * BeforeScenario Creates costumer and merchant and creates bankaccount with balance of 100
     * author Lucas
     * @throws BankServiceException_Exception
     */
    @Before
    public void beforeScenario() throws BankServiceException_Exception {
        User merchant = new User();
        User costumer = new User();
        costumer.setCprNumber("100000-1111");
        costumer.setFirstName("Ole");
        costumer.setLastName("Olsen");
        merchant.setLastName("Mads");
        merchant.setFirstName("Madsen");
        merchant.setCprNumber("200000-2222");

        consumerAccountID = bank.createAccountWithBalance(costumer, new BigDecimal(100));
        merchantAccountID = bank.createAccountWithBalance(merchant, new BigDecimal(100));
    }

    /**
     *  AfterScenario retires the bankaccounts and clears the tokens
     *  author Lucas
     *  @throws BankServiceException_Exception
     */
    @After
    public void afterScenario() throws BankServiceException_Exception {

        bank.retireAccount(consumerAccountID);
        bank.retireAccount(merchantAccountID);
        Response response = tokenManagerUrl.path("clearTokens/" + cpr).request().get(Response.class);
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        String tokens = getToken.readEntity(String.class);
        System.out.println("Reset request:" + response.getStatus());
        System.out.println("Reset tokens: " + tokens);
        System.out.println("Reset tokens: " + getToken.getStatus());
    }

    /**
     * author: Frederik
     * Creates user with identifier CPR from addCustomers
     * @param string
     */
    @Given("I create user with identifier {string}")
    public void i_create_user_with_identifier(String string) {
        AddCustomerBody body = new AddCustomerBody();
        body.cpr = string;
        body.name = "lucas";
        cpr = string;
        String output = accountManagerUrl.path("addCustomers").request().post(Entity.entity(body, MediaType.APPLICATION_JSON), String.class);
        System.out.println(output);
    }

    /**
     * Checks if user exists with status code: Ok
     * author: Frederik
     * @param string
     */
    @Then("User is created in the system with uniqID of {string}")
    public void user_is_created_in_the_system_with_uniqID_of(String string) {
        Response exists = accountManagerUrl.path("getCustomers/" + string).request().get(Response.class);
        System.out.println(exists.getStatus());
        assertEquals(200, exists.getStatus());
    }


    /**
     * Checks if a unique ID is in the system
     * author: Lucas
     * @param arg0
     */
    @Given("I am created in the system with the uniqID of {string}")
    public void iAmCreatedInTheSystemWithTheUniqIDOf(String arg0) {
        Response exists = accountManagerUrl.path("getCustomers/" + arg0).request().get(Response.class);
        System.out.println(exists.getStatus());
        assertEquals(200, exists.getStatus());
    }

    /**
     * Checks if user with ID has a unused token
     * author: Frederik
     * @param int1
     */
    //I requests tokens for the first time
    @Given("I have {int} unused token")
    public void iHaveUnusedToken(Integer int1) {
        Response requestToken = tokenManagerUrl.path("requestToken/" + int1 + "/" + cpr).request().get(Response.class);
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        System.out.println(getToken.getStatus());
        System.out.println("iHaveUnusedToken");
    }

    /**
     * Requests a specific amount of tokens
     * author: Lucas
     * @param int1
     */
    @When("I request {int} tokens")
    public void iRequestTokens(int int1) {
        // Write code here that turns the phrase above into concrete actions
        Response response = tokenManagerUrl.path("requestToken/" + int1 + "/" + cpr).request().get(Response.class);
        System.out.println(response.getStatus());
        System.out.println("iRequestTokens");
    }


    /**
     * Checks if user with ID has the specified tokens
     * author: Frederik
     * @param arg1
     */
    @Then("^I should have (\\d+) tokens$")
    public void iShouldHaveTokens(int arg1) {
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        System.out.println(getToken.getStatus());
        String[] tokens = getToken.readEntity(String[].class);
        System.out.println(tokens.length);
        assertEquals(arg1, tokens.length);
        System.out.println("iShouldHaveTokens");
    }

    /**
     * Requests token
     * author: Lucas
     * @param int1
     */
    @When("I request {int} token")
    public void iRequestToken(Integer int1) {
        Response response = tokenManagerUrl.path("requestToken/" + int1 + "/" + cpr).request().get(Response.class);
        System.out.println(response.getStatus());
        System.out.println("iRequestToken");
    }

    /**
     * Checks if user has unused tokens
     * author: Frederik
     * @param int1
     */
    @Given("I have {int} unused tokens")
    public void iHaveUnusedTokens(Integer int1) {
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        System.out.println(getToken.getStatus());
        System.out.println("iHaveUnusedTokens");
    }

    /**
     * Checks if user has more than specified unused tokens
     * author: Lucas
     * @param arg1
     */
    @Given("^I have more than (\\d+) unused tokens$")
    public void iHaveMoreThanUnusedTokens(int arg1) {
        Response getToken = tokenManagerUrl.path("requestToken/" + arg1 + 1 + "/" + cpr).request().get(Response.class);
        System.out.println(getToken.getStatus());
    }

    /**
     * TokenRequestDenied and gets error message
     * author: Frederik
     * @param arg1
     * @throws Exception
     */
    @Then("^I should get a TokenRequestDenied error message \"([^\"]*)\"$")
    public void iShouldGetATokenRequestDeniedErrorMessage(String arg1) throws Exception {

    }

    /**
     * Checks if user has less than specified amount unused tokens than
     * author: Lucas
     * @param arg1
     * @throws Exception
     */
    @Given("^I have less than (\\d+) unused tokens$")
    public void iHaveLessThanUnusedTokens(int arg1) throws Exception {
        Response getToken = tokenManagerUrl.path("requestToken/" + (arg1 - 1) + "/" + cpr).request().get(Response.class);
    }

    /**
     * gets Token and scans it in the tokenManager
     * author: Frederik
     */
    @When("Merchant scans my token")
    public void merchantScansMyToken() {
        // Write code here that turns the phrase above into concrete actions
        Response getToken = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        String[] token = getToken.readEntity(String[].class);
        Response verify = tokenManagerUrl.path("useToken/" + token[0] + "/" + cpr).request().get(Response.class);
    }

    @And("Requests a transfer of {int}kr")
    public void requestsATransferOfKr(int arg0) {
    }


    /**
     * Checks if the token is lost
     * author: Lucas
     */
    @Then("I lose my token and the money is withdrawn from my account")
    public void iLoseMyTokenAndTheMoneyIsWithdrawnFromMyAccount() {
        Response getAfter = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        String[] after = getAfter.readEntity(String[].class);
        assertEquals(1, after.length);
    }

    /**
     * Checks if token is lost and recieves an error message
     * author: Frederik
     * @param arg0
     */
    @Then("I lose my token and get the message {string}")
    public void iLoseMyTokenAndGetTheMessage(String arg0) {
        Response getAfter = tokenManagerUrl.path("getTokens/" + cpr).request().get(Response.class);
        String[] after = getAfter.readEntity(String[].class);
        assertEquals(1, after.length);
    }

    String token;

    /**
     * Checks used tokens for user
     * author: Mussab
     * @param int1
     */
    @Given("I have {int} used token")
    public void iHaveUsedToken(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        Response getToken = tokenManagerUrl.path("requestToken/" + int1 + "/" + cpr).request().get(Response.class);
        token = getToken.readEntity(String.class);
        tokenManagerUrl.path("useToken/" + token + "/" + cpr);
    }

    /**
     * Status from useToken gets a not found
     * author: Mussab
     * @param string
     */
    @Then("I get an error message {string}")
    public void iGetAnErrorMessage(String string) {
        Response response = tokenManagerUrl.path("useToken/" + token + "/" + cpr).request().get(Response.class);
        assertEquals(response.getStatus(), 404);
    }

    BigDecimal consumer_original;

    /**
     * Creates bankaccount with specified balance
     * author: Mussab
     * @param int1
     */
    @Given("I have {int} kr on my account")
    public void iHaveKrOnMyAccount(Integer int1) {
        consumer_original = new BigDecimal(int1);
        // Write code here that turns the phrase above into concrete actions
        try {
            System.out.println("2");
            bank.retireAccount(consumerAccountID);
            User customer = new User();
            customer.setCprNumber("100000-1111");
            customer.setFirstName("Ole");
            customer.setLastName("Olsen");
            consumerAccountID = bank.createAccountWithBalance(customer, new BigDecimal(int1));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Transfers money from consumer to merchant account
     * author: Erik
     * @param int1
     */
    @When("The merchant requests {int} kr payment")
    public void theMerchantRequestsKrPayment(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        BigDecimal merchant_price = new BigDecimal(int1);
        try {
            System.out.println("1");
            bank.transferMoneyFromTo(consumerAccountID, merchantAccountID, new BigDecimal(int1), "test");
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Success message when the balance has been updated
     * author: Mussab
     * @param string
     * @param int1
     */
    @Then("I would get a success message with {string}, and {int} kr will be subtracted from my account")
    public void iWouldGetASuccessMessageWithAndKrWillBeSubtractedFromMyAccount(String string, Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        BigDecimal customer_deducted_amount = new BigDecimal(0);
        try {
            System.out.println("3");
            System.out.println(consumer_original + " " +bank.getAccount(consumerAccountID).getBalance() );
            customer_deducted_amount = consumer_original.subtract(bank.getAccount(consumerAccountID).getBalance());
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        assertEquals(new BigDecimal(int1), customer_deducted_amount);
    }


    /**
     * Retires accounts then tries to create account with balance
     * author: Mussab
     * @param int1
     */
    @Given("I only have {int} kr on my account")
    public void iOnlyHaveKrOnMyAccount(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        consumer_original = new BigDecimal(int1);
        try {
            bank.retireAccount(consumerAccountID);
            User customer = new User();
            customer.setCprNumber("100000-1111");
            customer.setFirstName("Ole");
            customer.setLastName("Olsen");
            consumerAccountID = bank.createAccountWithBalance(customer, new BigDecimal(int1));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get request for balance recieves error message and balance stays at original balance
     * author: Mussab
     * @param string
     */
    @Then("I would get an error message with {string}, and my balance would stay the same")
    public void iWouldGetAnErrorMessageWithAndMyBalanceWouldStayTheSame(String string) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("4");
        BigDecimal customer_current_balance = new BigDecimal(0);
        try {
            System.out.println(consumer_original + "" + bank.getAccount(consumerAccountID).getBalance() );
            customer_current_balance = bank.getAccount(consumerAccountID).getBalance();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        assertEquals(consumer_original, customer_current_balance);
    }

    /**
     * Makes transfer and uses token
     * author: Mussab
     * @param arg0
     * @param arg1
     */
    @Given("I used a token and bought a bag for {int}kr from a Merchant with the Token {string}.")
    public void iUsedATokenAndBoughtABagForKrFromAMerchantWithTheToken(int arg0, String arg1) {
        logBody log = new logBody();
        try {
            bank.getAccount(consumerAccountID).setBalance(new BigDecimal(arg0 + 100));
            bank.transferMoneyFromTo(consumerAccountID, merchantAccountID, new BigDecimal(arg0), "test");
            log.customer = bank.getAccount(consumerAccountID).getUser().getCprNumber();
            log.merchant = bank.getAccount(merchantAccountID).getUser().getCprNumber();
            log.money = arg0;
            log.Token = arg1;
            logManagerUrl.path("addLogs/").request().post(Entity.entity(log, MediaType.APPLICATION_JSON), String.class);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Recieves log for latest transaction on for customerCPR
     * author: Erik
     */
    @Then("I should get a log of my latest transaction.")
    public void iShouldGetALogOfMyLatestTransaction() {
        try {
            String customerCPR = bank.getAccount(consumerAccountID).getUser().getCprNumber();
            logManagerUrl.path("getLog/" + customerCPR);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}

@XmlRootElement
class logBody implements Serializable {
    @XmlElement
    String Token;
    @XmlElement
    String customer;
    @XmlElement
    String merchant;
    @XmlElement
    float money;
}