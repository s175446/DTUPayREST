package unittest;

import gherkin.deps.com.google.gson.Gson;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@XmlRootElement
class AddBody {
    @XmlElement String cpr;
    @XmlElement String name;
}

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCustomer {

    WebTarget tokenManagerUrl, accountManagerUrl;
    Client client;
    AddBody body = new AddBody();
    Gson gson = new Gson();

    /**
     * Instantiates client and user with name and cpr
     * author: Erik
     */
    public TestCustomer(){
        client = ClientBuilder.newClient();
        tokenManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8383/");
        accountManagerUrl = client.target("http://fastmoney-07.compute.dtu.dk:8181/");
        body.name = "Bob";
        body.cpr = "1111";
    }

    /**
     * Requests tokens and assertion from the request amount and the list.size of tokens
     * author: Lucas
     */
    @Test
    public void requestAndGetTokens(){
        int tokenRequestAmount = 2;
        Response request = tokenManagerUrl.path("requestToken/" + tokenRequestAmount + "/" + body.cpr).request().get(Response.class);
        Response get = tokenManagerUrl.path("getTokens/" + body.cpr).request().get(Response.class);
        String json = get.readEntity(String.class);
        List list = gson.fromJson(json, List.class);
        assertEquals(tokenRequestAmount, list.size());
    }

    /**
     * Requests too many tokens and gets a bad request response
     * author: Erik
     */
    @Test
    public void requestTooManyTokens(){
        Response request = tokenManagerUrl.path("requestToken/" + 20 + "/" + body.cpr).request().get(Response.class);
        assertEquals(request.getStatusInfo(), Response.Status.BAD_REQUEST);
    }

    /**
     * Creates user
     * author: Mussab
     */
    @Test
    public void createUser(){
        String output = accountManagerUrl.path("addCustomers").request().post(Entity.entity(body, MediaType.APPLICATION_JSON), String.class);
    }

    /**
     * Gets name
     * author: Burhan
     */
    @Test
    public void getName(){
        Response exists = accountManagerUrl.path("getCustomers/" + body.cpr).request(MediaType.APPLICATION_JSON).get(Response.class);
        String json = exists.readEntity(String.class);
        System.out.println(json);
        AddBody test = gson.fromJson(json, AddBody.class);
        assertEquals("Bob", test.name);
    }

    /**
     * Gets CPR
     * author: Erik
     */
    @Test
    public void getCPR(){
        Response exists = accountManagerUrl.path("getCustomers/" + body.cpr).request(MediaType.APPLICATION_JSON).get(Response.class);
        String json = exists.readEntity(String.class);
        AddBody test = gson.fromJson(json, AddBody.class);
        assertEquals("1111", test.cpr);
    }
}